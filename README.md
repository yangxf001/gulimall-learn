# gulimall-learn

#### 介绍
尚硅谷谷粒 mall 项目学习总结
    这个项目主要是为了用于尚硅谷谷粒商城项目的学习交流，以下是我自己的总结与遇到的一些问题的解决方案
    项目的代码也已经上传到 gitee 上地址如下:<br/>
    [谷粒商城前端项目](https://gitee.com/yeyinzhu/ex-mall-web-front "前端项目")    
    [谷粒商城后端项目](https://gitee.com/zga/ex-mall "后端项目")    

#### ES6
[es6](./ES6.md)

#### Linux 相关总结


#### spring cloud 相关总结
[springcloud](./springcloud.md)

#### vue 相关总结
[vue](./vue.md)

#### vscode 使用相关总结
[vscode](./vscode.md)

#### 遇到问题总结
[问题解决](./problem.md)